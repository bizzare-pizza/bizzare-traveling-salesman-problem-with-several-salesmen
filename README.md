# Travelling Salesmen Problem

A desktop application for illustrating the orders distribution algorithm.

## Technologies used

  * Language: C++ (C++17)
  * Graphics: SFML 

## Installation guide 
```console
$ curl https://gitlab.com/bizzare-pizza/bizzare-traveling-salesman-problem-with-several-salesmen/raw/master/main.cpp > main.cpp
$ g++ -std=c++17 -lsfml-graphics -lsfml-window -lsfml-system -omain main.cpp
```
## Project structure

 |   File   |        Purpose       |
 |:--------:|:--------------------:|
 | main.cpp | The main file |
 |   test   | Directory with samples |
 
 
 ## Usage demo

  Orders, drivers and availble pickup points are marked as red, green and blue dots correspondingly.

 
  ![](sampleGIF.gif)

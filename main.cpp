#include <SFML/Graphics.hpp>
#include <iostream>
#include <cmath>
#include <vector>
#include <set>
#include <map>
#include <queue>
#include <iomanip>
#include <stack>
#include <sstream>
#include <fstream>
#include <iterator>
#include <algorithm>
#include <bitset>
#include <numeric>
#include <cassert>
#include <unordered_set>
#include <unordered_map>
#include <unistd.h>
#include <time.h>
using namespace std;

const int inf = 1000001;

struct point { //Структура объекта для списка водителей
    int type;
    //0 - cafe
    //1 - order
    //2 - driver
    double x;
    double y;
    point(int a, double b, double c) {type=a; x=b; y=c;}
};


#define maxn 200000
vector <sf::Color> driversColors; //Цвета путей водителей
vector <vector<point> > driversPaths; //Вектор списков для водителей
vector<point> cafes; //Список кафе
int driversAmount; //Кол-во водителей
int cafesAmount; // Кол-во заказов
int ox, oy; //Координаты заказа
int minTime, minCafe, minDriver, minj1, minj2; //Переменные содержат: оптимальное время, кафе, водителя, номер в пути после solve
int actionsAmount; //Количество действий (в online не нужно)
bool showPath = true; //true - показать пути, false - нет
bool isMove = false; //Если pause нажата - true
bool canTouch = true; //true - нельзя создавать объекты нажатием мыши, включено в readfile режиме
int nowAct = 0; //Номер текущего действия(readFile режим)
bool canExit = false; //true - остались незаконченые заказы в readFile режима
double sumDist = 0; //Суммарная дистанция, проеденная автомобилями
time_t start; //Время начала
string pathProg; //Содежит путь директории программы
bool canWork[maxn]; //canWort[i] = false - водитель не может работать
time_t ordStart[maxn]; //Время начала заказа
int ordNum = -1; //Номер текущего заказа
double midTime = 0; //Среднее время ожидания заказа
double DIST_PER_S = 100.0; //Скорость водителя
int ordersAmount = 0; //Общее количество водителей

int dist(int x1, int y1, int x2, int y2) { //Расстояние между двумя точками в декартовой системе координат
    return (sqrt((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2)));
}

double _time, uTime;

#define K1 0.5
#define K2 200.0

#define addOrder 0
#define addDriver 1
#define deleteDriver 2

#define MAXY 525

struct action { //Структура объекта для списка водителей
    int type;
    //0 - cafe
    //-... - order
    //2 - driver
    double x;
    double y;
    double Time;
    action(int a, double b, double c, double d) {type=a; x=b; y=c; Time = d;}
};

double nowTime() {
    return difftime( time(0), start);
}

vector <action> actions;

void read() { //Считывание информации readFile режим
    isMove = true;
    cout << "Enter sample test number : ";
    string fileName;
    cin >> fileName;
    fileName = pathProg + "test/" + fileName + ".txt";
    freopen(fileName.c_str(), "r", stdin);
    cin >> cafesAmount;
    for (int i = 0; i < cafesAmount; i++) {
        double x, y;
        cin >> x >> y;
        cafes.push_back(point(0, x, y));
    }
    cin >> actionsAmount;
    ordNum = -1;
    for (int i = 0; i < actionsAmount; i++) {
        string t;
        int ty;
        cin >> t >> ty;
        if (t == "addOrder") {
            double x, y;
            cin >> x >> y;
            actions.push_back(action(ordNum, x, y, ty));
            ordNum--;
        } else if (t == "addDriver") {
            double x, y;
            cin >> x >> y;
            actions.push_back(action(addDriver, x, y, ty));
        } else if (t == "deleteDriver") {
            int n;
            cin >> n;
            actions.push_back(action(deleteDriver, n - 1, 0, ty));
            
        }
    }
    ordNum = -1;
}

void solve() {//жесткий алгоритм с минимальной максимальной дистанцией
    minTime = inf;
    int nTime = 0;
    double px, py, tx, ty;
    int driveLen[driversAmount];
    for (int i = 0; i < driversAmount; i++) { //Для каждого водителя
        if (!canWork[i]) {
            continue;
        }
        for (int l = 0; l < cafesAmount; l++) { //Ищем кафе
            for (int j1 = 0; j1 < driversPaths[i].size(); j1++) { //Ищем позиция для "забора" из кафе
                for (int j2 = j1; j2 < driversPaths[i].size(); j2++) { //Ищем, после чего вставлять "отвоз заказа"
                    nTime = 0;
                    px = driversPaths[i][0].x;
                    py = driversPaths[i][0].y;
                    for (int jj = 0; jj < driversPaths[i].size(); jj++) {
                        tx = driversPaths[i][jj].x;
                        ty = driversPaths[i][jj].y;
                        nTime += dist(px, py, tx, ty);
                        px = driversPaths[i][jj].x;
                        py = driversPaths[i][jj].y;
                        if (jj == j1) {
                            tx = cafes[l].x;
                            ty = cafes[l].y;
                            nTime += dist(px, py, tx, ty);
                            px = cafes[l].x;
                            py = cafes[l].y;
                        }
                        if (jj == j2) {
                            tx = ox;
                            ty = oy;
                            nTime += dist(px, py, tx, ty);
                            px = ox;
                            py = oy;
                        }
                    }
                    
                    if (nTime < minTime) {
                        minTime = nTime;
                        minCafe = l;
                        minDriver = i;
                        minj1 = j1;
                        minj2 = j2;
                    }
                }
            }
        }
    }
}


void _setColor() { //Присваевает новому водителю цвет пути
    int r, g, b;
    r = rand() % 255;
    g = rand() % 205 + 50;
    b = rand() % 255;
    sf::Color color(r, g, b);
    driversColors.push_back(color);
}

void update() { //Обновляет информацию о водилах
    stringstream ss;
    for (int i = min(minj1, minj2); i < driversPaths[minDriver].size(); i++) {
        ss << driversPaths[minDriver][i].type << ' ' << driversPaths[minDriver][i].x << ' ' << driversPaths[minDriver][i].y << ' ';
        if (i == minj1) {
            ss << 0 << " " << cafes[minCafe].x << " " << cafes[minCafe].y << " ";
        }
        if (i == minj2) {
            ss << ordNum << " " << ox << " " << oy << " ";
            ordNum--;
        }
    }
    driversPaths[minDriver].push_back(point(0, 0, 0));
    driversPaths[minDriver].push_back(point(0, 0, 0));
    for (int i = min(minj1, minj2); i < driversPaths[minDriver].size(); i++) {
        double x, y; int t;
        ss >> t >> x >> y;
        driversPaths[minDriver][i] = point(t, x, y);
    }
}

#define MODE_NONE 0
#define MODE_CAFE 1
#define MODE_DRIVER 2

int vmode = MODE_NONE;

void addOrders(int amount) { //Добавляет n заказов
    if (cafesAmount * driversAmount == 0) {
        return;
    }
    ordersAmount += amount;
    srand(time(NULL));
    for (int i = 0; i < amount; i++) {
        int x, y;
        do {
            x = 10 + rand() % 780;
            y = 10 + rand() % 580;
        } while(y >= MAXY);
        ox = x;
        oy = y;
        solve();
        update();
        
    }
}

string str(double k) {
    stringstream ss;
    ss << k;
    string a;
    ss >> a;
    return a;
}

void addDrivers(int amount) {
    srand(time(NULL));
    for (int i = 0; i < amount; i++) {
        int x;
        int y;
        vector <point> v;
        x = rand() % 800;
        y = rand() % 600;
        v.push_back(point(2, x, y));
        driversPaths.push_back(v);
        canWork[driversAmount] = true;
        driversAmount++;
        _setColor();
    }
}

int main(int argc, const char * argv[])
{
    sf::CircleShape invisibleCircle(13);
    invisibleCircle.setFillColor(sf::Color(10,255,255));
    invisibleCircle.setOrigin(200,200);
    sf::Font fnt;
    fnt.loadFromFile("font.ttf");
    
    sf::Text btntxt[8];
    cout << "Mode (0 - read files / 1 - on-line test) : ";
    cin >> canTouch;
    if (!canTouch) {
        read();
    }
    pathProg = argv[0];
    pathProg = pathProg.substr(0, pathProg.size() - 4);
    start = time(0);
    sf::RenderWindow win(sf::VideoMode(800, 600), "Bizzare Pizza Algo Visual", sf::Style::Close);
    
    sf::Clock vtick;
    
    win.setFramerateLimit(60);
    
    sf::CircleShape vcar(10);
    vcar.setFillColor(sf::Color(0,255,0));
    vcar.setOrigin(10, 10);
    
    sf::CircleShape vcafe(10);
    vcafe.setFillColor(sf::Color(0,0,255));
    vcafe.setOrigin(10, 10);
    
    sf::CircleShape vcli(10);
    vcli.setFillColor(sf::Color(255,0,0));
    vcli.setOrigin(10, 10);
    
    string vtxt[] = {" CAFE", "DRIVER", " ORDER", " PATH", " PAUSE", " SPEED", "  EXIT", " GEN"};
    
    sf::RectangleShape buttons[8];
    for(int i = 0; i < 8; ++i)
    {
        buttons[i].setSize(sf::Vector2f(50, 50));
        buttons[i].setFillColor(sf::Color(50, 50, 50));
        buttons[i].setOutlineColor(sf::Color(0, 0, 0));
        buttons[i].setOutlineThickness(3);
        buttons[i].setPosition(sf::Vector2f(320 + 60 * i, 540));
        
        btntxt[i].setCharacterSize(14);
        btntxt[i].setColor(sf::Color(255, 255, 255));
        btntxt[i].setString(vtxt[i]);
        btntxt[i].setFont(fnt);
        btntxt[i].setPosition(buttons[i].getPosition()+sf::Vector2f(0,20));
    }
    
    //    x1 : 560 - 610
    //    y1 : 540 - 590
    //
    //    x2 : 620 - 670
    //    y2 : 540 - 590
    //
    //    x3 : 680 - 730
    //    y3 : 540 - 590
    //
    //    x4 : 740 - 790
    //    y4 : 540 - 590
    double sx[4] = {560, 620, 680, 740};
    double bx[4] = {610, 670, 730, 790};
    double sy[4] = {540, 540, 540, 540};
    double by[4] = {590, 590, 590, 590};
    
    while(win.isOpen())
    {
        
        sf::Event ev;
        while(win.pollEvent(ev))
        {
            if(ev.type == sf::Event::Closed) win.close();
            else if(ev.type == sf::Event::KeyReleased)
            {
                if(ev.key.code == sf::Keyboard::C)
                {
                    vmode = MODE_CAFE;
                }
                else if(ev.key.code == sf::Keyboard::D)
                {
                    vmode = MODE_DRIVER;
                }
                else
                {
                    vmode = MODE_NONE;
                }
            }
            else if(ev.type == sf::Event::MouseButtonPressed)
            {
                sf::Vector2f mpos(ev.mouseButton.x, ev.mouseButton.y);
                bool flag = true;
                int bNumber = -1;
                for (int i = 0; i < 8; i++)
                {
                    if(buttons[i].getGlobalBounds().contains(mpos))
                    {
                        flag = false;
                        switch(i)
                        {
                            case 0:
                                vmode=MODE_CAFE;
                                break;
                            case 1:
                                vmode=MODE_DRIVER;
                                break;
                            case 2:
                                vmode=MODE_NONE;
                                break;
                            case 3:
                                showPath = !showPath;
                                break;
                            case 4:
                                isMove = !isMove;
                                break;
                            case 5:
                                if(DIST_PER_S > 100)
                                {
                                    DIST_PER_S = 100;
                                }
                                else
                                {
                                    DIST_PER_S = 300;
                                }
                                break;
                            case 6:
                                win.close();
                                break;
                            case 7:
                                addOrders(100);
                        }
                    }
                }
                if (buttons[0].getGlobalBounds().left <= mpos.x && mpos.x <= 800 && buttons[0].getGlobalBounds().top <= mpos.y && mpos.y <= 600) {
                    flag = false;
                }
                if (flag && canTouch) {
                    vector <point> v;
                    switch(vmode)
                    {
                        case MODE_CAFE:
                            if (ev.mouseButton.y <= MAXY) {
                                cafes.push_back(point(0, ev.mouseButton.x, ev.mouseButton.y));
                                cafesAmount++;
                            }
                            break;
                        case MODE_DRIVER:
                            if (ev.mouseButton.y <= MAXY) {
                                v.push_back(point(2, ev.mouseButton.x, ev.mouseButton.y));
                                driversPaths.push_back(v);
                                canWork[driversAmount] = true;
                                driversAmount++;
                                _setColor();
                            }
                            break;
                        default:
                            
                            if (ev.mouseButton.y <= MAXY) {
                                if (driversAmount * cafesAmount != 0) {
                                    ox = ev.mouseButton.x;
                                    oy = ev.mouseButton.y;
                                    ordStart[abs(ordNum)] = nowTime();
                                    solve();
                                    update();
                                }
                            }
                    }
                }
            }
        }
        if (!canTouch && nowTime() == actions[nowAct].Time) {
            if (actions[nowAct].type < 0) {
                ox = actions[nowAct].x;
                oy = actions[nowAct].y;
                ordStart[abs(actions[nowAct].type)] = nowTime();
                solve();
                update();
            } else if (actions[nowAct].type == addDriver) {
                vector <point> v;
                v.push_back(point(2, actions[nowAct].x, actions[nowAct].y));
                driversPaths.push_back(v);
                canWork[driversAmount] = true;
                driversAmount++;
                _setColor();
            } else if (actions[nowAct].type == deleteDriver) {
                canWork[int(actions[nowAct].x)] = 0;
            }
            nowAct++;
        }
        
        sf::Time vtime = vtick.getElapsedTime();
        vtick.restart();
        if (isMove) {
            for (int i = 0; i < driversPaths.size(); ++i) {
                if (driversPaths[i].size() > 1)
                {
                    double xx = driversPaths[i][1].x - driversPaths[i][0].x;
                    double yy = driversPaths[i][1].y - driversPaths[i][0].y;
                    
                    double ln = sqrt(xx*xx + yy*yy);
                    double haveMove = vtime.asSeconds()*DIST_PER_S;
                    
                    while (haveMove >= ln && driversPaths[i].size() > 1) {
                        driversPaths[i][0] = driversPaths[i][1];
                        driversPaths[i][0].type = 2;
                        if (driversPaths[i][1].type < 0) {
                            midTime += difftime(nowTime(), ordStart[abs(driversPaths[i][1].type)]);
                        }
                        driversPaths[i].erase(next(driversPaths[i].begin()));
                        
                        haveMove -= ln;
                        sumDist += ln;
                        
                        if (driversPaths[i].size() > 1) {
                            xx = driversPaths[i][1].x - driversPaths[i][0].x;
                            yy = driversPaths[i][1].y - driversPaths[i][0].y;
                            ln = sqrt(xx*xx + yy*yy);
                        }
                        
                    }
                    
                    if (haveMove < ln && driversPaths[i].size() > 1) {
                        xx /= ln;
                        yy /= ln;
                        
                        xx*=vtime.asSeconds()*DIST_PER_S;
                        yy*=vtime.asSeconds()*DIST_PER_S;
                        sumDist += sqrt(xx * xx + yy * yy);
                        driversPaths[i][0].x += xx;
                        driversPaths[i][0].y += yy;
                    }
                    
                }
            }
        }
        
        win.clear(sf::Color(255,255,255));
        
        
        if (showPath) {
            for (int i = 0; i < driversAmount; i++) {
                for (int j = 0; j < driversPaths[i].size() - 1; j++) {
                    pair <int, int> dd[5] = {{0, -1}, {0, 1}, {0, 0}, {1, 0}, {-1, 0}};
                    for (int l = 0; l < 5; l++) {
                        sf::Vertex line1[] =
                        {
                            sf::Vertex(sf::Vector2f(driversPaths[i][j].x + dd[l].first, driversPaths[i][j].y + dd[l].second), driversColors[i]),
                            sf::Vertex(sf::Vector2f(driversPaths[i][j + 1].x + dd[l].first, driversPaths[i][j + 1].y + dd[l].second), driversColors[i])
                        };
                        win.draw(line1, 2, sf::Lines);
                    }
                }
            }
        }
        for (auto x : driversPaths)
            for (auto y : x) {
                if(y.type < 0) {
                    vcli.setPosition(sf::Vector2f(y.x, y.y));
                    win.draw(vcli);
                }
            }
        
        for (int i = 0; i < driversPaths.size(); ++i) {
            vcar.setPosition(sf::Vector2f(driversPaths[i][0].x, driversPaths[i][0].y));
            
            if (!(driversPaths[i].size() == 1 && canWork[i] == false)) {
                win.draw(vcar);
            }
        }
        for (int i = 0; i < cafes.size(); ++i) {
            vcafe.setPosition(sf::Vector2f(cafes[i].x, cafes[i].y));
            win.draw(vcafe);
        }
        for (int i = 0; i < 8; i++) {
            win.draw(buttons[i]);
        }
        for (int i = 0; i < 8; ++i) {
            win.draw(btntxt[i]);
        }
        
        
        canExit = true;
        for (int i = 0; i < driversAmount; i++) {
            if (driversPaths[i].size() > 1) {
                canExit = false;
                break;
            }
        }
        
        if (!canTouch && canExit && nowTime() >= actions[actionsAmount - 1].Time) {
            /*cout << "\nExit time = " << nowTime() << '\n';
             cout << "Summary distance = " << sumDist << '\n';
             midTime /= double(actionsAmount - driversAmount);
             cout << "Avg time = " << midTime << '\n';*/
            //            return 0;
        }
        
        win.draw(invisibleCircle);
        sf::Text distance_;
        distance_.setString("dist: " + str(floor(sumDist)));
        distance_.setFont(fnt);
        distance_.setCharacterSize(20);
        distance_.setColor(sf::Color(255, 0, 0));
        distance_.move(5, 570);
        win.draw(distance_);
        
        win.display();
    }
    return 0;
}
